import React,{Component} from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  import BuySell from './BuySell';
  import {toast} from 'react-toastify';
  import './Explore.css';

  
export default class Explore extends Component{

    
    
    constructor(props){
        super(props)
        this.state ={
            users:[],
            ticker:'',
            volume:'',
            buyalertmsg:" ",
            sellalertmsg:" ",
            set:" "
            
        }
        
    }
    

    changeHandler=(e)  =>{
        this.setState({
            ticker:e.target.value
      
            //[e.target.name]: e.target.value
        })
    }

    changeVolumeHandler=(e)  =>{
        this.setState({
            volume:e.target.value
        })
    }


    submitHandler = (e) => {
        e.preventDefault()
        //console.log(this.state.ticker)
        axios.get(`http://localhost:8080//api/stocks/buy_or_sell/${this.state.ticker}`, this.state)
        .then(response => {
            this.setState({users:response.data});
            this.setState({set:"true"});
        })
        
    }
    submitBuyHandler = (e) => {
        e.preventDefault()
        axios.get(`http://localhost:8080/api/stocks/buy/${this.state.ticker}/${this.state.volume}`, this.state)
        .then(response => {
            console.log(response);
            this.setState({buyalertmsg:"Bought stock"})
        }).catch(error =>{
            this.setState({buyalertmsg:"error"});
        });
        //this.notifyBuy()
        
    }
    submitSellHandler = (e) => {
        e.preventDefault()
        axios.get(`http://localhost:8080/api/stocks/sell/${this.state.ticker}/${this.state.volume}`, this.state)
        .then(response => {
            console.log(response);
            {response.data=='' ? this.setState({sellalertmsg:"Not sufficient to sell"}):this.setState({sellalertmsg:"sold stock"})}
        }).catch(error =>{
            this.setState({sellalertmsg:"error"});
        });
        
    }

    /*submitBuyHandler =(e) => {
        <Router>
        <Switch>
          <Route path='/buysell'  component={BuySell} />
         </Switch>
         </Router>
    }*/

     /*buy() {
         <Link to='/buy' component={BuySell}>Buy</Link>
        
    }*/


    render()
    {
        //const {userId} = this.state
    return (
        <div>
            <h1 className = "text-center">Search for stock details</h1>
        <div className="col">
        <form onSubmit={this.submitHandler}>
        <div className="col-2">
            <input 
            id='ticker'
            type='text' 
            name ='ticker'
            placeholder='Ticker' 
            value={this.state.ticker}
            onChange={this.changeHandler}
           ></input>
           <div class="bt">
           <button class="centre" type='submit'>View</button>
           </div>
           {this.state.set==" " ? null :<div>
                
             <table className = "table table-bordered">
                    <thead class="thead thead-dark" align-text="center">
                        <tr>
                            <th>Ticker</th>
                            <th>Company</th>
                            <th>Price</th>
                            <th>Day Low</th>
                            <th>Day High</th>
                            <th>Year Low</th>
                            <th>Year High</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                           
                           this.state.users.map(
                            user =>
                            <tr key = {user.symbol} className="text-centre">
                                <td>{user.symbol}</td>
                                <td>{user.name}</td>
                                <td>{user.price}</td>
                                <td>{user.dayLow}</td>
                                <td>{user.dayHigh}</td>
                                <td>{user.yearLow}</td>
                                <td>{user.yearHigh}</td>
                            </tr>
                        )
                         
                            }
                    </tbody>
                </table>
            </div>}
           </div>
        </form>
        </div>
        {this.state.set==" " ? null : <div className="col"> <form>
            <div className="col-2">
           <input 
            id='volume'
            type='text' 
            name ='volume'
            placeholder='Volume' 
            value={this.state.volume}
            onChange={this.changeVolumeHandler}
           ></input>
           <div className="bt">
        <button class="buy" onClick={this.submitBuyHandler}>Buy</button>
        <button class="sell" onClick={this.submitSellHandler}>Sell</button>
        
        {this.state.buyalertmsg=="Bought stock" ? <div class="alert alert-success" role="alert">
            {this.state.buyalertmsg}
        </div> : null }

        {this.state.sellalertmsg=="sold stock" ?<div class="alert alert-success" role="alert">
            {this.state.sellalertmsg}
        </div> : null}
        {this.state.sellalertmsg=="Not sufficient to sell" ?<div class="alert alert-danger" role="alert">
            {this.state.sellalertmsg} </div>: null }
         
        </div>
        </div>
        </form>
        </div> }  
        </div>
        /*
        //After adding button submit
        <div>
                
                <h1 className = "text-center">Trade History</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th>Ticker</th>
                            <th>Company</th>
                            <th>Price</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                           
                           this.state.users.map(
                            user =>
                            <tr key = {user.symbol}>
                                <td>{user.symbol}</td>
                                <td>{user.name}</td>
                                <td>{user.price}</td>
                            </tr>
                        )
                         
                            }
                    </tbody>
                </table>
            </div> 
        //
        <div>
            <form onSubmit={this.submitHandler}>
            <div>
                <input 
                id='ticker'
                type='text' 
                name ='ticker' 
                onChange={this.changeHandler}
               ></input>
            </div>
            <button type='submit'>Submit Now</button>
            </form>
            <div>
                
                <h1 className = "text-center">Trade History</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th> Ticker</th>
                            <th>DateTime</th>
                            <th>Volume</th>
                            <th>Value</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                            this.state.users.map(
                                user =>
                                <tr>
                                    <td>{user.symbol}</td>
                                    <td>{user.name}</td>
                                    <td>{user.price}</td>
                                </tr>
                            )
                            }
                    </tbody>
                </table>
            </div>
            <h1 className = "text-center">Trade History</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th> Id</th>
                            <th>Ticker</th>
                            <th>DateTime</th>
                            <th>Volume</th>
                            <th>Value</th>
                            <th>Buy/Sell</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                            this.state.users.map(
                                user =>
                                <tr key = {user.orderId}>
                                    <td>{user.orderId}</td>
                                    <td>{user.ticker}</td>
                                    <td>{user.dateTime}</td>
                                    <td>{user.volume}</td>
                                    <td>{user.valuePerStock}</td>
                                    <td>{user.buyOrSell}</td>
                                </tr>
                            )
                            }
                    </tbody>
                </table>
        </div>*/

    );
    }
}