import React, { useState, useEffect } from "react";
import { Bar, Line, Pie } from 'react-chartjs-2'
import axios from "axios";
const PieChart = () => {
  const [chartData, setChartData] = useState({});
  const [employeeSalary, setEmployeeSalary] = useState([]);
  const [employeeAge, setEmployeeAge] = useState([]);
  const chart = () => {
    let empRet = [];
    let empTic = [];
    axios
      .get("http://localhost:8080/api/stocks/update")
      .then(res => {
        console.log(res);
        for (const dataObj of res.data) {
          empRet.push(parseInt(dataObj.returns));
          empTic.push(dataObj.ticker);
        }
        setChartData({
          labels: empTic,
          datasets: [
            {
              label: "level of thiccness",
              data: empTic,
              backgroundColor: ["rgba(75, 192, 192, 0.6)"],
              borderWidth: 4
            }
          ]
        });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(empTic, empRet);
  };
  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App">
      <h1>Top 5 Companies</h1>
      <div>
        <Bar 
          data={chartData}
          options={{
            responsive: true,
            title: { text: "THICCNESS SCALE", display: true },
            scales: {
              yAxes: [
                {
                  ticks: {
                    autoSkip: true,
                    maxTicksLimit: 10,
                    beginAtZero: true
                  },
                  gridLines: {
                    display: false
                  }
                }
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false
                  }
                }
              ]
            }
          }}
        />
      </div>
    </div>
  );
};
export default PieChart;