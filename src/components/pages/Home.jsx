import React from 'react';
import Featuredinfo from "../featuredinfo/Featuredinfo";
import Charts from "../charts/Charts";
import "./Home.css";
import { userData } from "../../dummy"

export default class Home extends React.Component{
    render(){
    return (
        <div className="home">
            <Featuredinfo />
            <Charts grid />
        </div>
    );
    }
}

