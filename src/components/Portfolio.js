import React from 'react';
import PortfolioService from './PortfolioService';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Nav,Form,FormControl,Button}  from 'react-bootstrap';

class Portfolio extends React.Component{

    constructor(props){
        super(props)
        this.state ={
            users:[]
        }
        
    }
    componentDidMount(){
        PortfolioService.getUsers().then((Response) =>{
            this.setState({users:Response.data})
        });
    }
    render(){
        return(
            <div className="center">
                
                <h1 className = "text-center">Portfolio</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th>Ticker</th>
                            <th>volume</th>
                            <th>Total Price</th>
                            <th>Net Assets</th>
                            <th>Returns</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                            this.state.users.map(
                                user =>
                                <tr key = {user.ticker}>
                                    <td>{user.ticker}</td>
                                    <td>{user.volume}</td>
                                    <td>{user.totalPricePaid}</td>
                                    <td>{user.netAssetValue}</td>
                                    <td>{user.returns}</td>
                                </tr>
                            )
                            }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default Portfolio