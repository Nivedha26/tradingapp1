import React from 'react';
import "./featuredinfo.css";
import { ArrowDownward, ArrowUpward } from "@material-ui/icons";

export default class Featuredinfo extends React.Component {
    render(){
  return (
    <div className="featured">
      <div className="featuredItem">
        <span className="featuredTitle">Total Assets</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">$2,415<ArrowUpward  className="featuredIcon positive"/>
   </span>
        </div>
         </div>
      <div className="featuredItem">
        <span className="featuredTitle">Net Quantity</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">80</span>

        </div>
      </div>
      <div className="featuredItem">
        <span className="featuredTitle">Net Returns</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">$2,225<ArrowUpward className="featuredIcon"/></span>
        
        </div>
         </div>
    </div>
  );
 }
}